/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief This file is part of the Week 1 Application Assignment
 *
 * A simple program for getting the maximum, mean, median and mean of a dataset
 *
 * @author Raymond Ardee F. Soriano
 * @date September 9, 2020
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */ 

void print_statistics(unsigned char * n_ptr, unsigned int arr_size);
/**
 * @brief: Function for printing the statistics of the array
 *
 * @param n_ptr
 * desc: local variable for the unsigned char pointer parameter for the given data array
 *
 * @param arr_size
 * desc: local variable for the unsigned integer parameter for the size of the data array
 *
 * @return Null
 */

void print_array(unsigned char * n_ptr, unsigned int arr_size);
/**
 * @brief: Function for printing the given data array
 *
 * @param n_ptr
 * desc: local variable for the unsigned char pointer parameter for the given data array
 *
 * @param arr_size
 * desc: local variable for the unsigned interger parameter for the size of the data array
 *
 * @return Null
 */

unsigned char find_median(unsigned char * n_ptr, unsigned int arr_size);
/**
 * @brief: Function for finding the median of the data array
 *
 * @param n_ptr
 * desc: local variable for the unsigned char pointer parameter for the given data array
 *
 * @param arr_size
 * desc: local variable for the unsigned integer parameter for the size of the data array
 *
 * @return unsigned char for median value of the data array
 */

unsigned char find_mean(unsigned char * n_ptr, unsigned int arr_size);
/**
 * @brief: Function for finding the mean of the data array
 *
 * @param n_ptr
 * desc: local variable for the unsigned char pointer parameter for the given data array
 *
 * @param arr_size
 * desc: local variable for the unsigned integer parameter for the size of the data array
 *
 * @return unsigned char for the mean value of the data array
 */

unsigned char find_maximum(unsigned char * n_ptr, unsigned int arr_size);
/**
 * @brief: Function for finding the largest value in the data array
 *
 * @param n_ptr
 * desc: local variable for the unsigned char pointer parameter for the given data array
 *
 * @param arr_size
 * desc: local variable for the unsigned integer parameter for the size of the data array
 *
 * @return unsigned char for the largest value in the data array
 */

unsigned char find_minimum(unsigned char * n_ptr, unsigned int arr_size);
/**
 * @brief: Function for finding the smallest value in the data array
 *
 * @param n_ptr
 * desc: local variable for the unsigned char pointer parameter for the given data array
 *
 * @param arr_size
 * desc: local variable for the unsigned integer parameter for the size of the data array
 *
 * @return unsigned char for the smallest value in the data array
 */

unsigned char * sort_array(unsigned char * n_ptr, unsigned int arr_size);
/**
 * @brief: Function for sorting the data array from largest to smallest value
 *
 * @param n_ptr
 * desc: local variable for the unsigned char pointer parameter for the given data array
 *
 * @param arr_size
 * desc: local variable for the unsigned integer parameter for the size of the data array
 *
 * @return unsigned char pointer for the sorted array
 */

#endif /* __STATS_H__ */
