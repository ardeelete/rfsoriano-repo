/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief This file is the C file program for the Week 1 Application Assignment
 *
 * A simple program for getting the maximum, minimum, median and mean of a dataset.
 *
 * @author Raymond Ardee F. Soriano
 * @date September 9, 2020
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
  printf("\nThese are the values of the given array:");
  print_array(test, SIZE );
  printf("These are the  sorted values of the array:");
  print_array(sort_array(test, SIZE), SIZE);
  printf("These are the statistical values:\n");
  print_statistics(test, SIZE);
}


/* Function for printing the statistics of the array */
void print_statistics(unsigned char * n_ptr, unsigned arr_size){

  printf("The median value is %i.\n", find_median(sort_array(n_ptr, SIZE), SIZE));

  printf("The mean value is %i.\n", find_mean(n_ptr, SIZE));

  printf("The maximum value is %i.\n", find_maximum(n_ptr, SIZE));

  printf("The minimum value is %i.\n", find_minimum(n_ptr, SIZE));

  return;
}


/* Function for printing the given array */
void print_array(unsigned char * n_ptr, unsigned int arr_size){

  printf("\n[ ");
  for(int i=1; i<=SIZE; i++){

    unsigned int rem = i % 8;
    unsigned int n_element = n_ptr[i-1];

    /* For proper spacing of each element */
    if(n_element>99){
      printf("%i", n_element);
    }
    else if(n_element>9){
      printf(" %i", n_element);
    }
    else{
      printf("  %i", n_element);
    }

    /* For organizing print alignment, 8 elements per line */
    if(rem == 0){
      if(i==SIZE){
        printf("]\n\n");
      }
      else{
        printf(",\n  ");
      }
    }
    else{
      printf(", ");
    }
  }

  return;
}


/* Function for getting the median value of the given array */
unsigned char find_median(unsigned char * n_ptr, unsigned int arr_size){

  unsigned char median_val;
  /* Formula for median of odd-numbered array */
  if(SIZE%2>0){
    median_val = n_ptr[ (SIZE/2) ];
  }
  /* Formula for even-numbered array */
  else{
    median_val = (n_ptr[ (SIZE/2) ] + n_ptr[ (SIZE/2)-1 ]) / 2;
  }

  //return median value;
  return median_val;
}


/* Function for getting the mean of the given array */
unsigned char find_mean(unsigned char * n_ptr, unsigned int arr_size){

  unsigned int total_val = 0;
  /* Formula for mean of array*/
  for(int i=0; i<SIZE; i++){
    total_val+=n_ptr[i];
  }

  //return mean value;
  return total_val / 2;
}


/* Function for getting the largest value in the given array */
unsigned char find_maximum(unsigned char * n_ptr, unsigned int arr_size){

  /* Declaring 0 as highest initial value */
  unsigned char max_val = 0;

  for(int i=0; i<SIZE; i++){
    if(n_ptr[i] > max_val){
      max_val = n_ptr[i];
    }
  }

  /* Return highest value */
  return max_val;
}


/* Function for getting the largest value in the given array */
unsigned char find_minimum(unsigned char * n_ptr, unsigned int arr_size){

  /* Declaring 255 as lowest initial value*/
  unsigned char min_val = 255;

  for(int i=0; i<SIZE; i++){
    if(n_ptr[i] < min_val){
      min_val = n_ptr[i];
    }
  }

  /* Return lowest value */
  return min_val;
}


/* Function for sorting the array from largest to smallest */
unsigned char * sort_array(unsigned char * n_ptr, unsigned int arr_size){

  unsigned char temp;

  for(int i=0; i < SIZE; i++){
    for(int j=0; j < (SIZE - i - 1); j++){
      if( n_ptr[j] < n_ptr[j+1] ){
        temp = n_ptr[j];
        n_ptr[j] = n_ptr[j+1];
        n_ptr[j+1] = temp;
      }
    }
  }

  /* Return sorted array */
  return n_ptr;
}



/****
 EOF
****/
